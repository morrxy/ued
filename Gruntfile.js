module.exports = function(grunt) {

  // project configuration
  grunt.initConfig({
    less: {
      options: {
        compress: true
      },
      index: {
        files: {
          'css/index.css': 'src/less/index/index.less'
        }
      },
      view: {
        files: {
          'css/view.css': 'src/less/view/view.less'
        }
      }
    },

    // The connect task is used to serve static files with a local server.
    connect: {
      options: {
        port: 9000,
        livereload: 35729,
        hostname: '*' // * = accessible from anywhere ; default: localhost
      },
      livereload: {
        options: {
          open: true,
          base: [''] // '.tmp',
        }
      }
    },

    watch: {
      options: {
        livereload: '<%= connect.options.livereload %>'
      },
      index: {
        files: ['src/less/share/*', 'src/less/index/*'],
        tasks: ['less:index']
      },
      view: {
        files: ['src/less/share/*', 'src/less/view/*'],
        tasks: ['less:view']
      },
      html: {
        files: ['*.html']
      },
      gruntfile: {
        files: ['Gruntfile.js'],
        tasks: ['less']
      }
    }

  });

  // load the plugin that provides the "less" task
  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-contrib-connect');
  grunt.loadNpmTasks('grunt-contrib-watch');

  grunt.registerTask('server', ['connect', 'watch']);

  grunt.registerTask('default', ['server']);

};